
//
// Created by Mrisha Senior  on 2023-09-07.
//
/*
 Student Record System: Using C programming code create a program that allows users to input,
 edit, and display student records.
 You can include features like adding new students, deleting students, and calculating average grade
 */
#include <stdio.h>
#include <string.h>
#define MAX_STUDENTS 100
#define MAX_NAME_LEN 50
#define MAX_SUBJECTS 10
#define MAX_SUBJECT_NAME_LEN 30

typedef struct {
    char name[MAX_SUBJECT_NAME_LEN];
    float marks;
} Subject;

typedef struct {
    char name[MAX_NAME_LEN];
    char regNo[MAX_NAME_LEN];
    Subject subjects[MAX_SUBJECTS];
    int numSubjects;
} Mwanafunzi;

Mwanafunzi wanafunzi[MAX_STUDENTS];
int num_students = 0;

void addStudent() {
    if (num_students < MAX_STUDENTS) {
        printf("Ingiza Jina Kamili la Mwanafunzi: ");
        scanf("%49s", wanafunzi[num_students].name);
        printf("Ingiza namba ya usajili: ");
        scanf("%49s", wanafunzi[num_students].regNo);

        printf("Ingiza Idadi ya Masomo: ");
        int n;
        scanf("%d", &n);
        wanafunzi[num_students].numSubjects = n;

        for (int i = 0; i < n; i++) {
            printf("Ingiza Jina la Somo: ");
            scanf("%29s", wanafunzi[num_students].subjects[i].name);
            printf("Ingiza Marks za somo %s: ", wanafunzi[num_students].subjects[i].name);
            scanf("%f", &wanafunzi[num_students].subjects[i].marks);
        }
        num_students++;
    } else {
        printf("Idadi ya Mwisho ya Wanafunzi imefikiwa!\n");
    }
}

void displayStudents() {
    for (int i = 0; i < num_students; i++) {
        printf("\nStudent %d:\n", i + 1);
        printf("Jina: %s\n", wanafunzi[i].name);
        printf("Namba ya Usajili: %s\n", wanafunzi[i].regNo);
        printf("Masomo:\n");
        for (int j = 0; j < wanafunzi[i].numSubjects; j++) {
            printf("  %s: %.2f\n", wanafunzi[i].subjects[j].name, wanafunzi[i].subjects[j].marks);
        }
    }
}

void editStudent() {
    char regNoToEdit[MAX_NAME_LEN];
    printf("Ingiza namba ya usajili ili kuweza kubadili taarifa za mwanafunzi: ");
    scanf("%49s", regNoToEdit);

    for (int i = 0; i < num_students; i++) {
        if (strcmp(wanafunzi[i].regNo, regNoToEdit) == 0) {
            printf("Ingiza Jina Jipya: ");
            scanf("%49s", wanafunzi[i].name);
            printf("Ingiza namba mpya ya usajili: ");
            scanf("%49s", wanafunzi[i].regNo);

            printf("Ingiza Idadi ya Masomo: ");
            int n;
            scanf("%d", &n);
            wanafunzi[i].numSubjects = n;

            for (int j = 0; j < n; j++) {
                printf("Ingiza jina la somo: ");
                scanf("%29s", wanafunzi[i].subjects[j].name);
                printf("Ingiza Alama za Somo %s: ", wanafunzi[i].subjects[j].name);
                scanf("%f", &wanafunzi[i].subjects[j].marks);
            }
            return;
        }
    }
    printf("Taarifa za mwanafunzi hazijapatikana.\n");
}

void deleteStudent() {
    char regNoToDelete[MAX_NAME_LEN];
    printf("Ingiza namba ya usajili kama unataka kufuta taarifa za mwanafunzi: ");
    scanf("%49s", regNoToDelete);

    int index = -1;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(wanafunzi[i].regNo, regNoToDelete) == 0) {
            index = i;
            break;
        }
    }

    if (index != -1) {
        for (int i = index; i < num_students - 1; i++) {
            wanafunzi[i] = wanafunzi[i + 1];
        }
        num_students--;
        printf("Mwanafunzi amefutwa kikamilifu!\n");
    } else {
        printf("Mwanafunzi Hajapatikana.\n");
    }
}

void calculateAverage() {
    char regNo[MAX_NAME_LEN];
    printf("Ingiza namba ya usajili ya mwanafunzi: ");
    scanf("%49s", regNo);

    for (int i = 0; i < num_students; i++) {
        if (strcmp(wanafunzi[i].regNo, regNo) == 0) {
            float total = 0.0;
            for (int j = 0; j < wanafunzi[i].numSubjects; j++) {
                total += wanafunzi[i].subjects[j].marks;
            }
            printf("Wastani for %s: %.2f\n", wanafunzi[i].name, total / wanafunzi[i].numSubjects);
            return;
        }
    }
    printf("Mwanafunzi hajapatikana.\n");
}

int main() {
    int choice;
    while (1) {
        printf("\nMFUMO WA TAAFIRA ZA WANAFUNZI\n");
        printf("1. Add student\n");
        printf("2. Display students\n");
        printf("3. Edit student\n");
        printf("4. Delete student\n");
        printf("5. Calculate average marks\n");
        printf("6. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                addStudent();
                break;
            case 2:
                displayStudents();
                break;
            case 3:
                editStudent();
                break;
            case 4:
                deleteStudent();
                break;
            case 5:
                calculateAverage();
                break;
            case 6:
                return 0;
            default:
                printf("Chaguo sio Sahihi!\n");
        }
    }
}
